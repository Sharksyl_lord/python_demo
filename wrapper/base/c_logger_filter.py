# -*- coding: utf-8 -*- 
# @Time : 2021/10/4 下午12:08
# @Author :  施亚林
# @File : c_logger_filter.py
import logging
import sys


class LevelFilter(logging.Filter):
    # CRITICAL = 50
    # FATAL = CRITICAL
    # ERROR = 40
    # WARNING = 30
    # WARN = WARNING
    # INFO = 20
    # DEBUG = 10
    def __init__(self, logging_level):
        super().__init__()
        self.__logging_level = logging_level

    def filter(self, record: logging.LogRecord) -> bool:
        if self.__logging_level == logging.DEBUG:
            return record.levelno == logging.DEBUG
        elif self.__logging_level == logging.INFO:
            return record.levelno == logging.INFO
        elif self.__logging_level == logging.WARNING:
            return record.levelno == logging.WARNING
        elif self.__logging_level == logging.ERROR:
            return record.levelno == logging.ERROR
        elif self.__logging_level == logging.FATAL:
            return record.levelno == logging.FATAL


if __name__ == '__main__':
    log = logging.getLogger('test')
    log.setLevel(logging.DEBUG)
    stream_handler = logging.StreamHandler(sys.stdout)
    log.addHandler(stream_handler)
    log.warning('this is warning-1')
    log.info('this is info-1')

    log.addFilter(LevelFilter(logging.INFO))
    log.warning('this is warning-2')
    log.info('this is info-2')

    stream_handler.addFilter(LevelFilter(logging.INFO))
    log.warning('this is warning-3')
    log.info('this is info-3')
    log.warning('this is warning-abc')

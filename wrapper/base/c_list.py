# -*- coding: utf-8 -*- 
# @Time : 2021/9/26 下午9:13
# @Author :  施亚林
# @File : c_list.py
class CList:
    @classmethod
    def show_all_item(cls, list_obj):
        if len(list_obj) == 0:
            print('列表为空!')
        else:
            for item in list_obj:
                print(item)

# -*- coding: utf-8 -*- 
# @Time : 2021/9/29 下午4:18
# @Author :  施亚林
# @File : c_yaml.py
class CYaml:
    def __init__(self, config_data: dict):
        self.__data: dict = config_data

    def query_node_content(self, query: str):
        query_note_list = query.split('.')
        try:
            all_query_content = self.__data[query_note_list[0]]
            for query_note in query_note_list[1:]:
                all_query_content = all_query_content[query_note]
            query_result = all_query_content
        except Exception:
            raise Exception('节点[{0}]查询失败，请在配置文件中修正！'.format(query))

        return query_result

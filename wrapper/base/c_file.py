# -*- coding: utf-8 -*- 
# @Time : 2021/10/4 上午11:35
# @Author :  施亚林
# @File : c_file.py
import os


class CFile:
    @classmethod
    def cheack_directory_and_create(cls, path):
        try:
            if not os.path.exists(path):
                os.mkdir(path)
            return os.path.exists(path)
        except Exception as error:
            raise Exception('目录[{0}]创建异常，原因为：{1}'.format(path, error.__str__()))

    @classmethod
    def dirname(cls, __file__):
        return os.path.dirname(__file__)

    @classmethod
    def current_file_path_upper(cls, __file__):
        path = cls.dirname(__file__)
        return os.path.abspath(os.path.join(path, '../..'))

    @classmethod
    def join(cls, path, other):
        path = str(path)
        other = str(other)
        return os.path.join(path, other)
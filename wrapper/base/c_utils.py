# -*- coding: utf-8 -*- 
# @Time : 2021/9/27 下午10:07
# @Author :  施亚林
# @File : c_utils.py
import uuid


class CUtils:
    @classmethod
    def show_line_with_star(cls, num):
        print('*' * num)

    @classmethod
    def any_to_str(cls, any):
        return str(any)

    @classmethod
    def uuid(cls):
        uuid_str = CUtils.any_to_str(uuid.uuid1())
        uuid_text = uuid_str.replace('-', '')
        return uuid_text


if __name__ == '__main__':
    print(CUtils.uuid())

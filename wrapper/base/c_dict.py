# -*- coding: utf-8 -*- 
# @Time : 2021/10/7 下午3:20
# @Author :  施亚林
# @File : c_dict.py
from collections import OrderedDict


class CDict:
    @classmethod
    def orderdict(cls, dict_obj):
        # todo 将字典按照指定顺序输出，但占用空间为普通字典的两倍，另是否排序成功待测试
        try:
            dict_obj = dict(dict_obj)
            return OrderedDict(dict_obj)
        except Exception as error:
            raise Exception(error.__str__())


if __name__ == '__main__':
    datamining_appstore_pl_dict = {
        "id": "default",
        "name": "default",
        "title": "default",
        "path": "default",
        "class": "default",
        "type": "default",
        "type_title": "default",
    }
    for item in datamining_appstore_pl_dict.items():
        print(item)
    print(CDict.orderdict(datamining_appstore_pl_dict))

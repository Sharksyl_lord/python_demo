# -*- coding: utf-8 -*- 
# @Time : 2021/9/27 下午10:17
# @Author :  施亚林
# @File : c_logger.py
import logging
import logging.config
import threading

from config import config_logging
from wrapper.base.c_yaml import CYaml
from wrapper.base.c_file import CFile
from wrapper.base.c_logger_filter import LevelFilter
from wrapper.base.c_constant import CConstant


class CLogger:
    """
    CRITICAL = 50
    FATAL = CRITICAL
    ERROR = 40
    WARNING = 30
    WARN = WARNING
    INFO = 20
    DEBUG = 10
    NOTSET = 0
    """
    __output = False
    __instance_lock = threading.Lock()

    def __init__(self):
        if not self.__output:
            self.__yaml = CYaml(config_logging)
            self.__output_location = self.__yaml.query_node_content(CConstant.Config_Logger_Con_output_location)
            self.__logging_config = self.__yaml.query_node_content(CConstant.Config_Logger)
            self.__logger = None
            self.identify_output_location()
            logging.config.dictConfig(self.__logging_config)
            if self.__output_location == 'path':
                if self.__output:
                    self.__logger = logging.getLogger('logger')
                    for handler in self.__logger.root.handlers:
                        if handler.name == 'console':
                            continue
                        else:
                            handler.addFilter(LevelFilter(handler.level))

    def __new__(cls):
        if not hasattr(CLogger, '_instance'):
            with CLogger.__instance_lock:
                if not hasattr(CLogger, '_instance'):
                    CLogger._instance = object.__new__(cls)
        return CLogger._instance

    def debug(self, msg, *args, **kwargs):
        if self.__output and self.__logger is not None:
            self.__logger.debug(msg, *args, **kwargs)
        else:
            logging.debug(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        if self.__output and self.__logger is not None:
            self.__logger.info(msg, *args, **kwargs)
        else:
            logging.info(msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        if self.__output and self.__logger is not None:
            self.__logger.warning(msg, *args, **kwargs)
        else:
            logging.warning(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        if self.__output and self.__logger is not None:
            self.__logger.error(msg, *args, **kwargs)
        else:
            logging.error(msg, *args, **kwargs)

    def identify_output_location(self):
        logging_config = self.__logging_config
        if self.__output_location == 'console':
            pass
        elif self.__output_location == 'path':
            logging_output_path = logging_config['path']
            logging_output_path = CFile.join(CFile.current_file_path_upper(__file__), logging_output_path)
            handlers = logging_config['handlers']
            if CFile.cheack_directory_and_create(logging_output_path):
                for handler_name, handler_config in handlers.items():
                    filename = handler_config.get('filename', None)
                    if filename is None:
                        continue
                    if logging_output_path is not None:
                        handler_config['filename'] = logging_output_path + filename
                    else:
                        raise Exception('请检查[logging.config.handlers.{0}.filename]的配置'.format(handler_name))
        self.__output = True


if __name__ == '__main__':
    CLogger().debug('test,debug')
    CLogger().info('test,info')
    CLogger().warning('test,warning')
    CLogger().error('test,error')

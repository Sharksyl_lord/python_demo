# -*- coding: utf-8 -*- 
# @Time : 2021/10/5 上午10:38
# @Author :  施亚林
# @File : c_constant.py
class CConstant:
    """项目常量"""

    # 配置文件节点
    Config_Logger_Location = 0
    Name_Logger = 'logger'
    Config_Logger = '{0}.config'.format(Name_Logger)
    Config_Logger_Con_output_location = '{0}.output_location'.format(Config_Logger)
    Config_Logger_Con_version = '{0}.version'.format(Config_Logger)
    Config_Logger_Con_disable_existing_loggers = '{0}.disable_existing_loggers'.format(Config_Logger)
    Config_Logger_Con_root = '{0}.root'.format(Config_Logger)
    Config_Logger_Con_path = '{0}.path'.format(Config_Logger)
    Config_Logger_Con_formatters = '{0}.formatters'.format(Config_Logger)
    Config_Logger_Con_handlers = '{0}.handlers'.format(Config_Logger)
    Config_Logger_Con_handlers_console = '{0}.console'.format(Config_Logger_Con_handlers)
    Config_Logger_Con_handlers_debug = '{0}.debug'.format(Config_Logger_Con_handlers)
    Config_Logger_Con_handlers_info = '{0}.info'.format(Config_Logger_Con_handlers)
    Config_Logger_Con_handlers_warn = '{0}.warn'.format(Config_Logger_Con_handlers)
    Config_Logger_Con_handlers_error = '{0}.error'.format(Config_Logger_Con_handlers)

    Config_DataBases_Location = 1

    Name_DataBases = "databases"
    Name_DataBase = "database"
    Name_UserName = "username"
    Name_PassWord = "password"
    Name_Port = "port"
    Name_Host = "host"

    Name_ID = "id"
    Name_info = "info"
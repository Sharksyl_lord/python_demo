# -*- coding: utf-8 -*- 
# @Time : 2021/9/29 下午4:14
# @Author :  施亚林
# @File : __init__.py
import yaml
import os
from wrapper.base.c_constant import CConstant

filepath = os.path.join((os.path.dirname(__file__)), 'config.yaml')
with open(filepath) as file:
    configdata = list(yaml.safe_load_all(file))
    config_logging = configdata[CConstant.Config_Logger_Location]
    config_databases = configdata[CConstant.Config_DataBases_Location]
# print(config_databases)

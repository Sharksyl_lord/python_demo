# -*- coding: utf-8 -*- 
# @Time : 2021/10/5 上午10:35
# @Author :  施亚林
# @File : plane_bg.py
from plane_sprites import GameSprite
from study.oop.project_parctice.plane_war.base.c_pygame import *


class Background(GameSprite):
    """游戏背景精灵"""

    def __init__(self, image_name='images/background.png', speed=1, is_alt=False):
        super().__init__(image_name, speed)
        if is_alt:
            self.rect.y = -self.rect.height

    def update(self, *args, **kwargs) -> None:
        super().update()

        if self.rect.y >= SCREEN_RECT.height:
            self.rect.y = -self.rect.height

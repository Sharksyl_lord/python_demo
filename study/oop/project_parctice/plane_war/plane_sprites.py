# -*- coding: utf-8 -*- 
# @Time : 2021/10/5 上午9:52
# @Author :  施亚林
# @File : plane_sprites.py
import pygame


class GameSprite(pygame.sprite.Sprite):
    """飞机大战-游戏精灵"""

    def __init__(self, image_name: str, speed: int = 1):
        super().__init__()
        self.image = pygame.image.load(image_name)
        self.rect = self.image.get_rect()
        self.speed = speed

    def update(self, *args, **kwargs) -> None:
        # 在屏幕的垂直方向上移动
        self.rect.y += self.speed

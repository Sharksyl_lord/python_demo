# -*- coding: utf-8 -*- 
# @Time : 2021/10/5 下午5:11
# @Author :  施亚林
# @File : plane_bullet.py
from wrapper.base.c_logger import CLogger
from study.oop.project_parctice.plane_war.plane_sprites import GameSprite


class Bullet(GameSprite):
    """子弹精灵"""

    def __init__(self, image_name='images/bullet1.png', speed=-2):
        super().__init__(image_name, speed)

    def update(self, *args, **kwargs) -> None:
        super().update()

        if self.rect.bottom <= 0:
            CLogger().info('删除子弹对象')
            self.kill()

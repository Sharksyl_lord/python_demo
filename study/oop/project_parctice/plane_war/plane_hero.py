# -*- coding: utf-8 -*-或
# @Time : 2021/10/5 下午4:31
# @Author :  施亚林
# @File : plane_hero.py
import pygame.sprite

from study.oop.project_parctice.plane_war.base.c_pygame import *
from study.oop.project_parctice.plane_war.plane_sprites import GameSprite
from study.oop.project_parctice.plane_war.plane_bullet import Bullet


class Hero(GameSprite):
    """英雄精灵"""

    def __init__(self, image_name='images/me1.png', speed=0):
        super().__init__(image_name, speed)
        # 设置英雄初始位置
        self.rect.centerx = SCREEN_RECT.centerx
        self.rect.bottom = SCREEN_RECT.bottom - 60
        self.bullets = pygame.sprite.Group()

    def update(self, *args, **kwargs) -> None:
        self.rect.x += self.speed

        if self.rect.left <= SCREEN_RECT.left:
            self.rect.left = SCREEN_RECT.left
        elif self.rect.right >= SCREEN_RECT.right:
            self.rect.right = SCREEN_RECT.right

    def fire(self):
        for i in (0, 1, 2):
            # 创建子弹精灵
            bullet = Bullet()
            # 设置精灵位置
            bullet.rect.bottom = self.rect.top - i * 20
            bullet.rect.centerx = self.rect.centerx

            self.bullets.add(bullet)

# -*- coding: utf-8 -*- 
# @Time : 2021/10/4 下午7:16
# @Author :  施亚林
# @File : game_windows_pra.py
import pygame
from wrapper.base.c_logger import CLogger
from plane_sprites import GameSprite

pygame.init()

# 创建窗口
screen = pygame.display.set_mode((400, 700))

# 绘制背景
bg = pygame.image.load('images/background.png')
screen.blit(bg, (0, 0))

# 绘制英雄飞机
hero = pygame.image.load('images/me1.png')
screen.blit(hero, (150, 300))
pygame.display.update()
# 记录rect记录飞机的初始位置
hero_rect = pygame.Rect(150, 300, 102, 126)
# 创建时钟对象
clock = pygame.time.Clock()

# 创建敌机精灵
enemy = GameSprite('../images/enemy1.png')
enemy1 = GameSprite('../images/enemy2.png', 2)

# 创建敌机的精灵组
enemy_group = pygame.sprite.Group(enemy, enemy1)

while True:
    # 指定循环内部执行频率(刷新帧率) number次/s
    clock.tick(60)

    # 监听事件
    event_list = pygame.event.get()
    for event in event_list:
        if event.type == pygame.QUIT:
            pygame.quit()
            CLogger().info('游戏退出...')
            exit()  # 终止当前正在执行的程序

    # 修改飞机位置
    hero_rect.y -= 1
    hero_rect.x -= 1
    # 边界判断
    if hero_rect.y + hero_rect.height == 0:
        hero_rect.y = screen.get_rect()[3]
    elif hero_rect.y - hero_rect.width == screen.get_rect()[3]:
        hero_rect.y = 0
    if hero_rect.x + hero_rect.height == 0:
        hero_rect.x = screen.get_rect()[2]
    elif hero_rect.x - hero_rect.width == screen.get_rect()[2]:
        hero_rect.x = 0
    # 先修改背景
    screen.blit(bg, (0, 0))
    # 再修改飞机位置
    screen.blit(hero, hero_rect)

    # 更新精灵位置及绘制
    enemy_group.update()
    enemy_group.draw(screen)

    pygame.display.update()

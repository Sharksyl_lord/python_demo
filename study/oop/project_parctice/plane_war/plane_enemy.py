# -*- coding: utf-8 -*- 
# @Time : 2021/10/5 上午11:48
# @Author :  施亚林
# @File : plane_enemy.py
import random

import pygame.image
from wrapper.base.c_logger import CLogger
from study.oop.project_parctice.plane_war.plane_sprites import GameSprite
from study.oop.project_parctice.plane_war.base.c_pygame import *


class Enemy(GameSprite):
    def __init__(self, image_name='images/enemy1.png', speed=1):
        super().__init__(image_name, speed)
        image_name_order = random.randint(1, 2)
        self.image_name = 'images/enemy{0}.png'.format(image_name_order)
        self.image = pygame.image.load(self.image_name)
        self.speed = random.randint(1, 3)
        self.rect = self.image.get_rect()
        self.rect.bottom = 0
        self.rect.x = random.randint(0, SCREEN_RECT.width - self.rect.width)

    def update(self, *args, **kwargs) -> None:
        super().update()

        if self.rect.y >= SCREEN_RECT.height:
            CLogger().info('删除敌军对象')
            self.kill()


if __name__ == '__main__':
    print(Enemy().image_name)

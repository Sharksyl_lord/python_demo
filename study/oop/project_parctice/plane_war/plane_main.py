# -*- coding: utf-8 -*- 
# @Time : 2021/10/5 上午10:11
# @Author :  施亚林
# @File : plane_main.py
from wrapper.base.c_logger import CLogger
from study.oop.project_parctice.plane_war.base.c_pygame import *
from study.oop.project_parctice.plane_war.plane_bg import Background
from study.oop.project_parctice.plane_war.plane_enemy import Enemy
from study.oop.project_parctice.plane_war.plane_hero import Hero


class PlaneGame:
    """游戏主体类"""

    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode(SCREEN_RECT.size)
        self.clock = pygame.time.Clock()  # 游戏时钟*设置帧率
        self.__create_sprites()  # 创建精灵
        pygame.time.set_timer(ENEMY_EVENT, 1000)  # 定时器 ms
        pygame.time.set_timer(HERO_FIRE_EVENT, 500)  # 定时器 ms

    def start_game(self):
        CLogger().info("游戏开始---")

        while True:
            # 设置刷新帧率
            self.clock.tick(FRAME_PER_SEC)
            # 事件监听
            self.__event_hadler()
            # 碰撞检测
            self.__check_collide()
            # 更新/绘制精灵组
            self.__update_sprites()
            # 更新显示
            pygame.display.update()

    def __create_sprites(self):
        # 创建背景精灵及精灵组
        bg1 = Background()
        bg2 = Background(is_alt=True)
        self.back_group = pygame.sprite.Group(bg1, bg2)

        # 创建敌机精灵组
        self.enemy_group = pygame.sprite.Group()

        # 创建英雄的精灵和精灵组
        self.hero = Hero()
        self.hero_group = pygame.sprite.Group(self.hero)

    def __event_hadler(self):
        for event in pygame.event.get():
            # 判断是否退出游戏
            if event.type == pygame.QUIT:
                PlaneGame.__game_over()
            elif event.type == ENEMY_EVENT:
                # 创建敌机精灵
                enemy = Enemy()

                # 添加精灵至精灵组
                self.enemy_group.add(enemy)
            elif event.type == HERO_FIRE_EVENT:
                self.hero.fire()
            # elif event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
            #     self.hero.rect.x += 1
        # 使用键盘提供的方法获取键盘按键， 返回按键元组
        keys_pressed = pygame.key.get_pressed()
        # 判断元组中对应的按键索引值 1按下
        if keys_pressed[pygame.K_RIGHT]:
            self.hero.speed = 2
        elif keys_pressed[pygame.K_LEFT]:
            self.hero.speed = -2
        else:
            self.hero.speed = 0

    def __check_collide(self):
        # 子弹摧毁敌机
        pygame.sprite.groupcollide(self.hero.bullets, self.enemy_group, True, True)
        # 敌机摧毁英雄
        # 摧毁敌机
        enemy_list = pygame.sprite.spritecollide(self.hero, self.enemy_group, True)
        # 摧毁英雄
        if len(enemy_list) > 0:
            self.hero.kill()
            self.__game_over()

    def __update_sprites(self):
        self.back_group.update()
        self.back_group.draw(self.screen)

        self.enemy_group.update()
        self.enemy_group.draw(self.screen)

        self.hero_group.update()
        self.hero_group.draw(self.screen)
        self.hero.bullets.update()
        self.hero.bullets.draw(self.screen)

    @staticmethod
    def __game_over():
        CLogger().info("游戏结束---")

        pygame.quit()
        exit()


if __name__ == '__main__':
    game = PlaneGame()
    game.start_game()

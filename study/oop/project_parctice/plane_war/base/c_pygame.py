# -*- coding: utf-8 -*- 
# @Time : 2021/10/7 下午9:20
# @Author :  施亚林
# @File : c_pygame.py
import pygame

# 飞机大战
SCREEN_RECT = pygame.Rect(0, 0, 457, 700)  # 屏幕大小的常量
FRAME_PER_SEC = 60  # 刷新帧率
ENEMY_EVENT = pygame.USEREVENT  # 敌机定时器
HERO_FIRE_EVENT = pygame.USEREVENT + 1  # 英雄发射子弹事件

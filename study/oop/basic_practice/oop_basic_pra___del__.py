# -*- coding: utf-8 -*-
# @Time : 2021/9/25 下午3:10
# @Author :  施亚林
# @File : oop_basic_pra___del__.py
from study.oop.base_item.c_animal import CAnimal
from wrapper.base.c_utils import CUtils
from wrapper.base.c_logger import CLogger


class Cat(CAnimal):
    def __del__(self):
        print('删除对象')

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    def eat(self):
        CLogger().info('{0}吃鱼'.format(self._name))

    def drink(self):
        CLogger().info('{0}喝水'.format(self._name))


if __name__ == '__main__':
    cat = Cat('猫', '田园猫')
    cat.eat()
    cat.drink()
    CUtils.show_line_with_star(10)
    cat.name = '橘猫'
    cat.eat()
    cat.drink()
    CLogger().info(cat.name)
    CUtils.show_line_with_star(10)
    # del cat
    CLogger().info('十六进制地址：{0}'.format(cat))
    CLogger().info('十进制地址:{0}'.format(id(cat)))
    # del cat
    CUtils.show_line_with_star(10)

# -*- coding: utf-8 -*- 
# @Time : 2021/9/25 下午4:36
# @Author :  施亚林
# @File : oop_basic_pra___str__.py
from study.oop.basic_practice.oop_basic_pra___del__ import Cat
from wrapper.base.c_logger import CLogger


class Cat1(Cat):
    pass


class Cat2(Cat):
    def __str__(self):
        CLogger().info('^-^.Cat2.__str__()')


if __name__ == '__main__':
    cat1 = Cat1('猫', 'cat1')
    cat1.eat()
    print(cat1.__str__())

    cat2 = Cat2('猫', 'cat2')
    cat2.eat()
    print(cat2.__str__())

# -*- coding: utf-8 -*- 
# @Time : 2021/9/25 下午5:43
# @Author :  施亚林
# @File : oop_basic_pra_house.py
from study.oop.base_item.house.c_house import CHouse
from wrapper.base.c_list import CList
from wrapper.base.c_utils import CUtils
from wrapper.base.c_logger import CLogger


class MyHouse(CHouse):
    def info(self):
        CLogger().info(self.__str__() + '\n家具：')
        CList.show_all_item(self._item_list)


if __name__ == '__main__':
    house = MyHouse('四合院', 262.5)
    house.info()
    CUtils.show_line_with_star(10)
    house.add_item('床', 16.2)
    house.add_item('桌子', 5.4)
    house.add_item('凳子', 10.4)
    CUtils.show_line_with_star(10)
    house.show_item()
    CUtils.show_line_with_star(10)
    house.info()
    CUtils.show_line_with_star(10)
    # house.add_item('凳子', 1000.4)

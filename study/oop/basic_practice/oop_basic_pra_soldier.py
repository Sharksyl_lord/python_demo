# -*- coding: utf-8 -*- 
# @Time : 2021/9/27 下午9:58
# @Author :  施亚林
# @File : oop_basic_pra_soldier.py
from study.oop.base_item.soldiers.soldier import CSoldier
from study.oop.base_item.soldiers.base.c_gun import CGun


class Solider(CSoldier):
    pass


if __name__ == '__main__':
    gun_ak = CGun('AK-47', 75)
    solider = Solider('lord', gun_ak)
    solider.info()
    solider.get_bullet()
    solider.fire()
    solider.info()
    solider.fire(99)
    solider.info()

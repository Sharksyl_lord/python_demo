# -*- coding: utf-8 -*- 
# @Time : 2021/9/25 下午5:22
# @Author :  施亚林
# @File : oop_basic_pra_person.py
from study.oop.base_item.c_person import CPerson
from wrapper.base.c_logger import CLogger


class CPersonTest(CPerson):
    def eat(self):
        self._weight += 1

    def run(self):
        self._weight -= 0.5

    def info(self):
        CLogger().info('{0}的体重为：{1:.2f} kg'.format(self._name, self._weight))


if __name__ == '__main__':
    xiaoming = CPersonTest('小明', 75.82365465)
    xiaoming.info()
    xiaoming.eat()
    xiaoming.info()
    xiaoming.run()
    xiaoming.info()


# enconding="utf-8
import time
import threading
from wrapper.base.c_logger import CLogger


g_num = 100

def test1():
    global g_num
    g_num += 1
    CLogger().info(" in test 1 g_num={0}".format(g_num))


def test2():
    CLogger().info(" in test 2 g_num={0}".format(g_num))

def main():
    t1 = threading.Thread(target=test1)
    t2 = threading.Thread(target=test2)
    CLogger().info("before the main Thread executes g_num={0}".format(g_num)) 
    t1.start()

    time.sleep(1)

    t2.start()
    time.sleep(1)
    CLogger().info("after the main Thread executes g_num={0}".format(g_num)) 

if __name__ == "__main__":
    main()

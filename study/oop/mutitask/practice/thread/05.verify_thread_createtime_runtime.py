# encoding="utf-8"
import time
import threading
from wrapper.base.c_logger import CLogger


def test1():
    # 如果创建Thread时执行的函数运行结束，则对应子线程结束...
    for i in range(5):
         CLogger().info("---test1---{0}---".format(i))
         time.sleep(1)

def main():
    CLogger().info(threading.enumerate())
    t1 = threading.Thread(target=test1)


    CLogger().info(threading.enumerate())
    t1.start()
    CLogger().info(threading.enumerate())


if __name__ == "__main__":
    main()

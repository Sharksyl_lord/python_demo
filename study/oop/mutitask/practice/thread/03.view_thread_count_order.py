# encoding="utf-8"
import time
import threading
from wrapper.base.c_logger import CLogger


def test1():
    for i in range(5):
     CLogger().info("---test1---{0}---".format(i))

def test2():
    for i in range(5):
        CLogger().info("---test1---{0}---".format(i))

def main():
    t1 = threading.Thread(target=test1)
    t2 = threading.Thread(target=test1)

    t1.start()
    time.sleep(1)
    CLogger().info("---1---")
    t2.start()
    time.sleep(1)
    CLogger().info("---2---")

    CLogger().info("{0}".format(threading.enumerate()))



if __name__ == "__main__":
    main()

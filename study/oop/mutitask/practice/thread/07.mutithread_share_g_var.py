# enconding="utf-8
import time
import threading
from wrapper.base.c_logger import CLogger

def test1(temp):
    temp.append(33)
    CLogger().info(" in test 1 g_num={0}".format(temp))


def test2(temp):
    CLogger().info(" in test 2 g_num={0}".format(temp))

g_nums = [11, 22]

def main():
    t1 = threading.Thread(target=test1, args=(g_nums,))
    t2 = threading.Thread(target=test2, args=(g_nums,))
    CLogger().info("before the main Thread executes g_num={0}".format(g_nums)) 
    t1.start()
    time.sleep(1)

    t2.start()
    time.sleep(1)
    CLogger().info("after the main Thread executes g_num={0}".format(g_nums)) 

if __name__ == "__main__":
    main()

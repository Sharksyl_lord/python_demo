import time
import threading
from wrapper.base.c_logger import CLogger 


def sing():
    """唱歌5秒"""
    for i in range(5):
        CLogger().info("---正在唱---")
        time.sleep(1)

def dump():
    """唱歌5秒"""
    for i in range(5):
        CLogger().info("---正在跳舞---")
        time.sleep(1)

def main():
    t1 = threading.Thread(target=sing)
    t2 = threading.Thread(target=dump)
    t1.start()
    t2.start()

if __name__ == "__main__":
    main()

from multiprocessing import Pool
import os, time , random

from wrapper.base.c_logger import CLogger


def worker(msg):
    t_start = time.time()
    CLogger().debug('[{0}]开始执行，进程PID[{1}]'.format(msg, os.getpid()))

    # random.random() 随机生成0～1之间的浮点数
    time.sleep(random.random()*2)
    t_stop = time.time()

    CLogger().debug('执行结束，耗时{0:.2f}s'.format(t_stop - t_start))


po = Pool(3) # 定义最大进程数为3 的进程池

CLogger().info("-----start-----"
        )
for i in range(0, 10):
    po.apply_async(worker, (i,))


po.close() # 关闭进程池，关闭后不再接收新的请求
po.join() # 等待进程池中所有子进程完成，必须放在close语句之后
CLogger().info("-----end-----")


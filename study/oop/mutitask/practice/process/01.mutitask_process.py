import threading
import multiprocessing
import time

from wrapper.base.c_logger import CLogger

def test1():
    while True:
        CLogger().debug(('1......'))
        time.sleep(1)

def test2():
    while True:
        CLogger().debug(('2......'))
        time.sleep(1)


def main():
 #    t1 = threading.Thread(target=test1)
 #    t2 = threading.Thread(target=test2)
 #    
 #    t1.start()
 #    t2.start() 
 

     t1 = multiprocessing.Process(target=test1)
     t2 = multiprocessing.Process(target=test2)
     
     t1.start()
     t2.start() 


if __name__ == '__main__':
    main()

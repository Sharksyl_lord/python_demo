# encoding=utf-8
import os, multiprocessing

from wrapper.base.c_logger import CLogger


def copy_file(q, file_name, old_folder_name, new_folder_name, new_folder_path):
    '''
    文件复制
    '''
    old_f = open(old_folder_name + '/' + file_name, 'rb')
    content = old_f.read()

    new_f = open(new_folder_path + '/' + file_name, 'wb')
    new_f.write(content)

    old_f.close()
    new_f.close()
    # 每复制完成一个文件就将其放入队列
    q.put(file_name)

def main():
    # 1.获取待复制文件夹的名称
    old_folder_name = input('请输入待复制文件夹路径:')
    # 2.创建目标文件夹
    try:
        new_folder_name = 'copy'
        new_folder_path = old_folder_name + new_folder_name
        if not os.path.exists(new_folder_path):
            os.mkdir(new_folder_path)
        else:
            CLogger().info('目标文件夹[{0}]已存在，无需创建!'.format(new_folder_path))
    except Exception as error:
       CLogger().error(error.__str__())
    # 3.获取文件夹中所有文件的名称
    file_names_list = os.listdir(old_folder_name)
    CLogger().debug(file_names_list[:5])
    # 4.创建进程池
    po = multiprocessing.Pool(5)
    
    # 5.创建队列
    q = multiprocessing.Manager().Queue()
    # 6.向进程池中添加文件复制任务
    for file_name in file_names_list:
        po.apply_async(
                copy_file, 
                args=(q, file_name, old_folder_name, new_folder_name, new_folder_path)
        )
        
    
    po.close()
    # po.join()
    all_count = len(file_names_list)
    finished_count = 0
    while True:
        file_name = q.get()
        finished_count += 1
        print('\r复制进度[{1:.2f}%]'.format(file_name, finished_count * 100 / all_count), end="")
        if finished_count >= all_count:
            print()
            break

if __name__ == '__main__':
    main()

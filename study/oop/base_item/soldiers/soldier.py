# -*- coding: utf-8 -*- 
# @Time : 2021/9/27 下午9:05
# @Author :  施亚林
# @File : soldier.py
from study.oop.base_item.soldiers.base.c_gun import CGun
from wrapper.base.c_logger import CLogger


class CSoldier:
    def __init__(self, name: str, gun: CGun):
        self.name = name
        self.gun = gun

    def info(self):
        CLogger().info('士兵:{0}\t枪：{1}\t子弹：{2}'.format(self.name, self.gun.model, self.gun.bullet))

    def get_bullet(self):
        if self.gun.bullet <= 0:
            CLogger().warning('士兵[{0}]子弹不足，请补充！'.format(self.name))
        else:
            CLogger().info('士兵[{0}]\t子弹：{1}'.format(self.name, self.gun.bullet))

    def fire(self, bullet_shout:int  = 1):
        if self.gun.bullet > 0 and bullet_shout <= self.gun.bullet:
            self.gun.bullet -= bullet_shout
        else:
            CLogger().warning('枪[{0}]子弹不足，请补充！'.format(self.gun.model))

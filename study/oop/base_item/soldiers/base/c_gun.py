# -*- coding: utf-8 -*- 
# @Time : 2021/9/27 下午8:59
# @Author :  施亚林
# @File : c_gun.py
from wrapper.base.c_logger import CLogger


class CGun:
    def __init__(self, model: str, bullet: int):
        self.model = model
        if bullet > 0:
            self.bullet = bullet
        else:
            raise Exception('子弹数必须大于0且为正整数！')

    def add_bullet(self, num: int):
        self.bullet += num

    def shoot(self):
        if self.bullet <= 0:
            CLogger().warning('子弹不足!')
            self.bullet = 0
        else:
            self.bullet -= 1

# -*- coding: utf-8 -*- 
# @Time : 2021/9/25 下午5:16
# @Author :  施亚林
# @File : c_person.py
from abc import abstractmethod


class CPerson:
    def __init__(self, name: str, weight: float):
        self._name = name
        self._weight = weight

    @property
    def weight(self):
        return self._weight

    @property
    def name(self):
        return self._name

    @abstractmethod
    def eat(self):
        self._weight += 1

    @abstractmethod
    def run(self):
        self._weight -= 0.5

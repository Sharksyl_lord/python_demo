# -*- coding: utf-8 -*- 
# @Time : 2021/9/25 下午5:41
# @Author :  施亚林
# @File : c_house.py
from study.oop.base_item.house.base.c_houseitem import CHouseItem
from wrapper.base.c_list import CList


class CHouse:
    def __init__(self, house_type: str, area: float):
        self.house_type = house_type
        self.area = area
        self.free_area = area
        self._item_list = []

    def __str__(self):
        if len(self._item_list) == 0:
            return ("户型：{0}\n总面积：{1:.2f}\n剩余:{2:.2f}\n家具:{3}"
                .format(self.house_type, self.area, self.free_area, '无'))
        else:
            return ("户型：{0}\n总面积：{1:.2f}\n剩余:{2:.2f}"
                    .format(self.house_type, self.area, self.free_area))

    def info(self):
        print(self.__str__())
        CList.show_all_item(self._item_list)

    def show_item(self):
        if len(self._item_list) == 0:
            print('未添加家具或内容！')
        else:
            for item in self._item_list:
                print(item)

    def add_item(self, name, area):
        if area > self.free_area:
            raise Exception("房间[{0}]剩余自由空间不足！".format(self.house_type))

        self._item_list.append(CHouseItem(name, area))
        self.free_area -= area
        return True

# -*- coding: utf-8 -*- 
# @Time : 2021/9/25 下午5:41
# @Author :  施亚林
# @File : c_houseitem.py
class CHouseItem:
    def __init__(self, name: str, area: float):
        self.name = name
        self.area = area

    def __str__(self):
        return '[{0}] 占地 {1:.2f}'.format(self.name, self.area)


if __name__ == '__main__':
    test = CHouseItem('托尔斯泰', 4.2)
    print(test)

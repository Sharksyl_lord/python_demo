# -*- coding: utf-8 -*- 
# @Time : 2021/9/25 下午4:46
# @Author :  施亚林
# @File : c_animal.py
from abc import abstractmethod


class CAnimal:
    def __init__(self, race: str, name: str):
        self._race = race
        self._name = name

    @abstractmethod
    def eat(self):
        pass

    @abstractmethod
    def drink(self):
        pass

# -*- coding: utf-8 -*- 
# @Time : 2021/10/4 下午4:45
# @Author :  施亚林
# @File : 捕获错误类型.py
from wrapper.base.c_logger import CLogger


class CException:
    @classmethod
    def demo01(cls):
        while True:
            try:
                num = int(input("输入一个整数:"))
                result = 8 / num
                CLogger().info('{0:.3}'.format(result))
                break
            except ZeroDivisionError:
                CLogger().error('除0错误！')
            except ValueError:
                CLogger().error('请输入正确的整数！')

    @classmethod
    def demo02(cls):
        while True:
            try:
                num = int(input("输入一个整数:"))
                result = 8 / num
                CLogger().info('{0:.3}'.format(result))
                break
            except Exception as error:
                CLogger().error('{0}'.format(error.__str__()))

    @classmethod
    def demo03(cls):
        while True:
            try:
                num = int(input("输入一个整数:"))
                result = 8 / num
                CLogger().info('{0:.3}'.format(result))
                break
            except Exception as error:
                CLogger().error('{0}'.format(error.__str__()))
            finally:
                CLogger().info('是否异常均会执行！')

    @classmethod
    def demo04(cls):
        num = int(input("输入一个整数:"))
        result = 8 / num
        CLogger().info('{0:.3}'.format(result))

    @classmethod
    def exception_catch_all(cls):
        """
        捕获主程序所有异常，利用此方法可在子程序中尽量少的捕捉异常
        待子程序异常传递至主程序时进行捕捉
        :return:
        """
        while True:
            try:
                cls.demo04()
                break
            except Exception as error:
                CLogger().error(error.__str__())

    @classmethod
    def input_password(cls):
        pwd = input("请输入密码：")
        if len(pwd) >= 8:
            return pwd
        else:
            raise Exception('密码长度小于八位！')

    @classmethod
    def password(cls):
        while True:
            try:
                cls.input_password()
                CLogger().info('密码设置成功！')
                break
            except Exception as error:
                CLogger().error(error.__str__())


if __name__ == '__main__':
    # CException.demo01()
    # CException.demo02()
    # CException.demo03()
    # CException.exception_catch_all()
    CException.password()

# -*- coding: utf-8 -*- 
# @Time : 2021/10/3 上午11:11
# @Author :  施亚林
# @File : 多值参数.py
from wrapper.base.c_logger import CLogger


class MulParameters:
    @classmethod
    def demo(cls, num, *args, **kwargs):
        CLogger().info(num)
        CLogger().info(args)
        CLogger().info(kwargs)

    @classmethod
    def sub_numbers(cls, *args):
        num = 0
        print(args)
        for i in args:
            num += i
        return num


if __name__ == '__main__':
    dict_obj = {
        "name": "Lord",
        "age": 25,
        "gender": "男"
    }
    MulParameters.demo(1, 2, 3, "str", dict_obj, name="test", age=84)
    print(MulParameters.sub_numbers(1, 2, 3))
    MulParameters.demo(1, **dict_obj)

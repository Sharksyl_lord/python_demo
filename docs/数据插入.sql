-- 加载拓展
CREATE EXTENSION pgcrypto;
CREATE EXTENSION "uuid-ossp";

-- 删除表
drop table if exists jd_goods;
-- 创建一个京东商品表
create table if not exists jd_goods (
    jg_id varchar(100) primary key not null,
    name varchar(150) not null,
    cate_name varchar(100) not null, -- category
    brand_name varchar(40) not null,
    price decimal(10,3) not null default 0, -- 一共十为数，三位小数
    is_show boolean not null default TRUE,
    is_saleoff boolean not null default False
);
-- alter table jd_goods alter column cate_name type varchar(100);

-- alter table jd_goods add foreign key (cate_name) references jd_goods_cates(jg_cate_id); -- 添加外键约束
-- alter table jd_goods add foreign key (brand_name) references jd_goods_brands(jg_brand_id); -- 添加外键约束
-- alter table jd_goods drop constraint if exists "jd_goods_brand_name_fkey"; -- 删除外键约束 外键会极大降低表个更新效率
-- alter table jd_goods drop constraint if exists "jd_goods_cate_name_fkey"; -- 删除外键约束

comment on table jd_goods is '京东商品信息';
comment on column jd_goods.jg_id is '商品ID';
comment on column jd_goods.name is '商品信息';
comment on column jd_goods.cate_name is '商品分类';
comment on column jd_goods.brand_name is '品牌名称';
comment on column jd_goods.price is '商品价格';
comment on column jd_goods.is_show is '是否展示';
comment on column jd_goods.is_saleoff is'是否售空';

insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'r510vc 15.6英寸笔记本','笔记本','华硕','3399');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'y400n 14.0英寸笔记本电脑','笔记本','联想','4999');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'g150th 15.6英寸游戏本','游戏本','雷神','8499');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'x550cc 15.6英寸笔记本','笔记本','华硕','2799');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'x240 超极本','超级本','联想','4880');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'u330p 13.3英寸超极本','超级本','联想','4299');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'svp13226scb 触控超极本','超级本','索尼','7999');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'ipad mini 7.9英寸平板电脑','平板电脑','苹果','1998');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'ipad air 9.7英寸平板电脑','平板电脑','苹果','3388');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'ipad mini 配备 retina 显示屏','平板电脑','苹果','2788');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'ideacentre c340 20英寸一体电脑 ','台式机','联想','3499');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'vostro 3800-r1206 台式电脑','台式机','戴尔','2899');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'imac me086ch/a 21.5英寸一体电脑','台式机','苹果','9188');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'at7-7414lp 台式电脑 linux ）','台式机','宏碁','3699');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'z220sff f4f06pa工作站','服务器/工作站','惠普','4288');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'poweredge ii服务器','服务器/工作站','戴尔','5388');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'mac pro专业级台式电脑','服务器/工作站','苹果','28888');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'hmz-t3w 头戴显示设备','笔记本配件','索尼','6999');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'商务双肩背包','笔记本配件','索尼','99');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'x3250 m4机架式服务器','服务器/工作站','ibm','6888');
insert into jd_goods values(replace(gen_random_uuid()::text, '-', ''),'商务双肩背包','笔记本配件','索尼','99');

-- 创建商品分类表
create table if not exists jd_goods_cates(
    jg_cate_id   varchar(100) primary key not null,
    jg_cate_name varchar(40)              not null
);

comment on table jd_goods_cates is '京东商品分类';
comment on column jd_goods_cates.jg_cate_id is '京东商品分类ID';
comment on column jd_goods_cates.jg_cate_name is '京东商品分类标题';

insert into jd_goods_cates(jg_cate_id, jg_cate_name)
select replace(gen_random_uuid()::text, '-', ''), cate_name from jd_goods group by cate_name;

insert into jd_goods_cates(jg_cate_id, jg_cate_name)
values (replace(gen_random_uuid()::text, '-', ''), '路由器'),
       (replace(gen_random_uuid()::text, '-', ''), '交换机'),
       (replace(gen_random_uuid()::text, '-', ''), '网卡');

-- 在创建数据表的时候一起插入数据
-- 注意: 需要对brand_name 用as起别名，否则name字段就没有值
create table jd_goods_brands (
    jg_brand_id varchar(100)  primary key not null ,
    jg_brand_name varchar(40) not null
);

comment on table jd_goods_brands is '京东商品品牌';
comment on column jd_goods_brands.jg_brand_id is '京东商品品牌ID';
comment on column jd_goods_brands.jg_brand_name is '京东商品品牌名称';

insert into jd_goods_brands(jg_brand_id, jg_brand_name)
select replace(gen_random_uuid()::text, '-', ''), brand_name from jd_goods group by brand_name;

insert into jd_goods_brands(jg_brand_id, jg_brand_name)
values (replace(gen_random_uuid()::text, '-', ''), '海尔'),
       (replace(gen_random_uuid()::text, '-', ''), '清华同方'),
       (replace(gen_random_uuid()::text, '-', ''), '神舟');

-- 同步表数据
update jd_goods
set cate_name = jd_goods_cates.jg_cate_id from jd_goods_cates
where cate_name = jd_goods_cates.jg_cate_name;
;

update jd_goods
set brand_name = jd_goods_brands.jg_brand_id from jd_goods_brands
where brand_name = jd_goods_brands.jg_brand_name;
;
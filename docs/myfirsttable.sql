-- auto-generated definition
create table if not exists test
(
    test_id         varchar(100) not null
        constraint test_pkey
            primary key,
    test_int        integer,
    test_boolean    boolean   default false,
    test_jsonb      jsonb     default '{
      "key": "value"
    }'::jsonb,
    test_date       date      default now(),
    test_timestamp  timestamp default now(),
    test_xml        xml,
    test_time       time      default now(),
    test_random_int integer   default round((random() * (10000)::double precision))
);

alter table test
    owner to postgres;

CREATE EXTENSION pgcrypto;
CREATE EXTENSION "uuid-ossp";

-- 生成uuid -- test_id
SELECT replace(gen_random_uuid()::text, '-', '');

-- 随机数
SELECT random();

insert into test(test_id, test_int)
select replace(gen_random_uuid()::text, '-', ''):: varchar(100) as test_id,
       generate_series(1, 200)::int                             as test_int;

update test
set test_timestamp = now(),
    test_time      = now(),
    test_boolean   = True
where test_random_int > 5000;

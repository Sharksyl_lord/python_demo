# python_demo

用于python知识回顾 sudo apt-get python3-libname

## python面向对象部分代码

1. 目录
    1. python_demo/OOP --- 面向对象代码练习
    2. python_demo/py_cookbook --- Python Cookbook书籍代码练习
        1. /chapter 章节

2. 内置方法和属性

   |序号|方法名|类型|作用|
                              |---|---|---|---|
   |01|\_\_del__|方法|<b>对象被从内存中销毁</b>前，会被<b>自动</b>调用|
   |02|\_\_str__|方法|返回<b>对象的描述信息</b>，print函数输出使用|
   |03|\_\_name__|属性|记录模块导入时信息|

    1. \_\_del__方法
        * 在```python```中
        * 当使用```类名（）```创建对象时，为对象分配完空间后，自动调用```__init__```方法
        * 当一个<b>对象被从内存中销毁</b>前，会自动调用```__del__```方法
        * 应用场景
            * ```__init__```改造初始化方法，可以让创建对象更加灵活
            * ```__del__```如果希望在对象被小会前，再做一些事情，可以考虑下```__del__```方法
        * 生命周期
            * 一个对象从调用```类名()```创建，生命周期开始
            * 一个对象的```__del__```方法一旦被调用，生命周期结束
            * 在对象的生命周期内，可以访问对象属性，或者让对象调用方法
    2. \_\_str__方法
        * 在```python```中，使用```print```输出对象变量，默认情况下，会输出这个变量<b>引用的对象</b>是<b>由哪一个类</b>创建的对象，以及<b>在内存中的地址(实录进制表示)</b>
        * 如果在开发中，希望使用```print```输出<b>对象变量</b>时，能够打印<b>自定义的内容</b>，就可以利用```__str__```内置方法
          
          > 注意：```__str__```方法必须返回一个字符串
        
    3. \_\_name__属性
        * \_\_name__是```python```的一个内置属性，记录着一个字符串
        * 如果<b>是被其他文件导入的</b>，```__name__```就是模块名
        * 如果<b>是当前执行的程序</b>```__name__```就是```__main__```


3. run_python.sh

    1. 使用方法
        1. 在终端中使用cd命令进入sh文件所在目录，使用sh命令启动sh文件

           ![img.png](image/run_python_sh_01.png)

        2. 将py文件相对根目录的路径，粘贴在终端输入处，回车执行。

           ![img.png](image/run_python_sh_02.png)

## 数据库

### linux

#### 数据库终端连接

1. 命令

> 切换角色：sudo -i -u postgres
>
> 链接数据库： psql
>
> 列举数据库： \l 相当于 show databases
>
> 切换数据库：\c <dbname>  相当于 use dbname
>
> 列举表：\dt 相当于 show tables
>
> 查看表结构： \d tablename 相当于 desc
>
> 查看编码： \encoding
>
> 退出psql： \q
>
> -- 从第八个记录开始，选择四个数据
>
> ```postgresql
> select test_id from test
> where test_boolean = TRUE
> limit 4 offset 8;
> ```
>
> 自连接查询省、市、县
> ```postgresql
> select china.title as province_title,
>        china.code  as province_code,
>        a.title     as city_title,
>        a.code      as city_code,
>        b.title     as county_title,
>        b.code      as county_code
>  from china
>           left join china as a on a.pid = china.code
>           left join china as b on a.code = b.pid
> where b.title = '南乐县';
> ```
>

2. 其他

    1. 解决： role "lord" does not exist
       > 创建角色：create role <role_name> superuser
       > 赋予登录权限：alter role lord login;
    1. 配置数据库以允许远程连接访问
       > 安装完成后，默认只能本地才能连接数据库，其他机子无法访问，需要进行配置。
       >
       > (以下示例开放了最大连接权限，实际配置根据需要而定）
        1. 修改监听地址
           > 将 #listen_addresses = 'localhost' 的注释去掉并改为 listen_addresses = '*'
           >
           >sudo gedit /etc/postgresql/9.5/main/postgresql.conf
        1. 修改可访问用户的IP段
           > 在文件末尾添加： host all all 0.0.0.0 md5 ，表示允许任何IP连接
           >
           > sudo gedit /etc/postgresql/12/main/pg_hba.conf
        1. 重启数据库
           
           > sudo /etc/init.d/postgresql restart
       
    1. 生成测试数据
        ```postgresql
       -- 加载拓展
        CREATE EXTENSION pgcrypto;
        CREATE EXTENSION "uuid-ossp";
        
        -- 生成uuid -- test_id
        SELECT replace(gen_random_uuid()::text, '-', '');
        
        -- 随机数  
        SELECT random();
        
        insert into test(test_id, test_int)
        select replace(gen_random_uuid()::text, '-', ''):: varchar(100) as test_id,
               generate_series(1, 200)::int as test_int;
        
        update test
        set test_timestamp = now(),
            test_time = now(),
            test_boolean = True
            where test_random_int > 5000;
       ```

    1. psql: could not connect to server: No such file or directory Is the server running
       > sudo service postgresql stop --force
       >
       >psql: could not connect to server: No such file or directory Is the server running
       >
       >卸载：sudo apt-get --purge remove postgresql\*
    >
    1. 宝塔面板
       > 外网面板地址: http://36.112.187.139:8888/7f7a56b6
       >
       >内网面板地址: http://192.168.64.129:8888/7f7a56b6
       > 
       > username: frrdwhjs
       >
       > password: 9d372ea1

    1. pycharm2021.1
       
       > wget https://download.jetbrains.8686c.com/python/pycharm-professional-2021.2.1.tar.gz

1. vim的常用命令

    | 命令 | 说明           |
    | ---- | -------------- |
    | yy | 复制光标所在行 |
    | p    | 粘贴 |
    |dd|删除/剪切所在行|
    |V|按行选中|
    |u|撤销|
    |ctrl+r|反撤销|
    |>>|向右缩进|
    |<<|向左缩进|
    |:/搜索的内容|搜索制定内容|
    |:%s/要替换的内容/替换后的内容/g|全剧替换|
    |:开始行数,结束行数s/要替换的内容/替换后的内容|局部替换|
    |.|重复上一次的命令操作|
    |G|回到最后一行|
    |gg|回到第一行|
    |数字+G|回到指定行|
    |ctrl+f|下一屏|
    |ctrl+b|上一屏|
	| :normal i # | 多行注释，V选中行 |


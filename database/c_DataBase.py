# -*- coding: utf-8 -*- 
# @Time : 2021/11/21 下午4:15
# @Author :  施亚林
# @File : c_DataBase.py
import psycopg2

from config import config_databases
from database.base.c_DBBase import CDBBase
from wrapper.base.c_yaml import CYaml
from wrapper.base.c_constant import CConstant
from wrapper.base.c_utils import CUtils


class CDataBase(CConstant):
    def __init__(self, db_type='postgres', db_id=0):
        self.__type = db_type
        self.__id = db_id
        self._yaml: CYaml = CYaml(config_databases)
        self._db_info = self.give_me_db_info()

    def give_me_db_info(self):
        config_list = self._yaml.query_node_content('{0}.{1}'.format(self.Name_DataBases, self.__type))
        if len(config_list) == 0:
            raise Exception('数据库[{0}.{1}]未配置！'.format(self.__type, self.__id))
        db_info = self.__search_db_info_by_id(config_list)
        return db_info

    def __search_db_info_by_id(self, config_list: list):
        for db_info in config_list:
            if db_info[self.Name_ID] == self.__id:
                return db_info

    def give_me_db(self):
        db_info = self.give_me_db_info()
        try:
            conn = psycopg2.connect(
                database=db_info[self.Name_DataBase],
                user=db_info[self.Name_UserName],
                password=db_info[self.Name_PassWord],
                host=db_info[self.Name_Host],
                port=db_info[self.Name_Port]
            )
            return CDBBase(conn)
        except Exception as error:
            raise Exception(error.__str__())


if __name__ == '__main__':
    result = CDataBase().give_me_db().execute(
        '''
        insert into jd_goods_brands values('{0}', '{1}')
        '''.format(
            CUtils.uuid(),
            "测试"
        )
    )

    result1 = CDataBase().give_me_db().all_rows(
        '''
        select * from jd_goods_brands
        where jg_brand_name='{0}';
        '''.format('苹果')
    )

    from wrapper.base.c_logger import CLogger
    CLogger().info(result)
    CLogger().info(result1)

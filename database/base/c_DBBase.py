# -*- coding: utf-8 -*- 
# @Time : 2021/11/21 下午4:15
# @Author :  施亚林
# @File : c_DBBase.py
from wrapper.base.c_constant import CConstant


class CDBBase(CConstant):
    def __init__(self, conn):
        # 创建连接
        self._conn = conn
        self._cursor = self._conn.cursor()

    def __del__(self):
        # 关闭连接
        self._conn.close()
        self._cursor.close()

    def execute(self, sql, parmas=None):
        """
        执行非查询语句
        :param sql:
        :param parmas:
        :return:
        """
        self._cursor.execute(sql)
        # 提交做出的更改
        self._conn.commit()
        return self._cursor.rowcount

    def all_rows(self, sql, parmas=None):
        """
        执行查询语句
        :param sql:
        :param parmas:
        :return:
        """
        self._cursor.execute(sql)
        if self._cursor.rowcount > 0:
            return self._cursor.fetchall()
        else:
            return self._conn.rowcount
